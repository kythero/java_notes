import contants.Constants;
import menu.MenuBuilder;
import panels.PanelBuilder;

import java.awt.*;
import java.util.Random;
import javax.swing.*;


public class FrameBuilder {

    private FrameBuilder(){};

    public static FrameBuilder getInstance(){
        return new FrameBuilder();
    }

    public void build(){
        JFrame frame = new JFrame(Constants.TITLE);
        frame.setLayout(new BorderLayout());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBackground(Constants.BLACK);
        frame.setPreferredSize(new Dimension(Constants.PREFERRED_WIDTH, Constants.PREFERRED_HEIGHT));
        frame.setLocation(new Random().nextInt(200), new Random().nextInt(200));
        frame.setVisible(true);

        frame.setJMenuBar(MenuBuilder.getInstance().getMenuBar());

        frame.add(PanelBuilder.northPanel, BorderLayout.NORTH);
        frame.add(PanelBuilder.westPanel, BorderLayout.WEST);
        frame.add(PanelBuilder.southPanel, BorderLayout.SOUTH);
        frame.add(PanelBuilder.centerPanel, BorderLayout.CENTER);

        frame.pack();
    }
}
