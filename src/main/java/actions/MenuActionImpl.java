package actions;

import contants.Constants;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;


public class MenuActionImpl extends AbstractAction {

    private String command;

    private MenuActionImpl(String command){
        super();
        this.command = command;
    }

    public static MenuActionImpl getInstance(String command){
        return new MenuActionImpl(command);
    }

    public void actionPerformed(ActionEvent event){
        switch (this.command){
            case Constants.MENU_NEW: {
                String fileName = JOptionPane.showInputDialog(Constants.CREATE_NEW_FILENAME);
                if (!fileName.isEmpty()){
                    try {
                        String path = String.join("\\", Constants.DOCS_PATH, fileName);
                        new File(path).mkdirs();
                    } catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
                break;
            }
        }

    }

}
