package actions;

import contants.Constants;
import panels.PanelBuilder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NorthPanelActionImpl extends AbstractAction {

    private String command;

    private NorthPanelActionImpl(String command){
        super(command);
        this.command = command;
    }

    public static NorthPanelActionImpl getInstance(String command){
        return new NorthPanelActionImpl(command);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        switch (this.command){
            case Constants.MENU_NEW:{
                String nameFile = JOptionPane.showInputDialog(Constants.CREATE_NEW_FILENAME);
                if(!nameFile.isEmpty()){
                    try {
                        String newFile = String.join("/", Constants.DOCS_PATH, nameFile);
                        Files.createDirectories(Paths.get(newFile));
                        PanelBuilder.getComboBox().addItem(newFile);
                        PanelBuilder.getConsole().consoleWrite("New doc has been created.");
                    } catch (IOException ex){
                        PanelBuilder.getConsole().consoleWrite(ex.getMessage());
                    }
                }
                break;
            }
            case Constants.BUTTON_SAVE:{
                try {
                    Files.createDirectories(PanelBuilder.getSelectedCategoryPath());
                    Files.write(PanelBuilder.getSelectedFilePath(), PanelBuilder.getMain().getText().getBytes());
                } catch (IOException ex) {
                    PanelBuilder.getConsole().consoleWrite(ex.getMessage());
                }
                break;
            }
            case Constants.BUTTON_REMOVE: {
                //remove only file in category.
                int selectedIndex = PanelBuilder.getComboBox().getSelectedIndex();
                if(selectedIndex != -1){
                    Path filePath = PanelBuilder.getSelectedFilePath();
                    if (Files.exists(filePath)){
                        try {
                            Files.delete(filePath);
                            PanelBuilder.getList().remove(selectedIndex);
                        } catch (IOException ex) {
                            PanelBuilder.getConsole().consoleWrite(ex.getMessage());
                        }
                    }
                }
                break;
            }
        }
    }
}
