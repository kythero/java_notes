import java.awt.EventQueue;

public class Notes {

    public static void main(String[] args){

        EventQueue.invokeLater(() -> {
            FrameBuilder frame = FrameBuilder.getInstance();
            frame.build();
        });
    }
}