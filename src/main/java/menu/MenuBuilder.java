package menu;

import actions.MenuActionImpl;
import contants.Constants;
import actions.NorthPanelActionImpl;

import javax.swing.*;


public class MenuBuilder {

    private MenuBuilder(){}

    public static MenuBuilder getInstance(){
        return new MenuBuilder();
    }

    public JMenuBar getMenuBar(){
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(getFileBar());
        menuBar.add(getEditBar());
        menuBar.add(getHelpBar());

        return menuBar;
    }

    private JMenu getFileBar(){
        JMenu menu = new JMenu(Constants.MENU_FILE);
        menu.add(new JMenuItem(NorthPanelActionImpl.getInstance(Constants.MENU_NEW)));
        menu.add(new JMenuItem(MenuActionImpl.getInstance(Constants.MENU_OPEN)));
        menu.add(new JMenuItem(MenuActionImpl.getInstance(Constants.MENU_SAVE)));
        menu.addSeparator();
        menu.add(new JMenuItem(MenuActionImpl.getInstance(Constants.MENU_EXIT)));

        return menu;
    }

    private JMenu getEditBar(){
        JMenu menu = new JMenu(Constants.MENU_EDIT);

        return menu;
    }

    private JMenu getHelpBar(){
        JMenu menu = new JMenu(Constants.MENU_HELP);

        return menu;
    }
}
