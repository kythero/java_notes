package contants;


import java.awt.*;

public class Constants {

    public static String TITLE;
    public static String VERSION;

    public static int PREFERRED_WIDTH;
    public static int PREFERRED_HEIGHT;

    public static String MENU_FILE = "File";
    public static final String MENU_NEW = "New";
    public static final String MENU_OPEN = "Open";
    public static final String MENU_SAVE = "Save";
    public static final String MENU_EXIT = "Exit";
    public static final String MENU_EDIT = "Edit";
    public static final String MENU_HELP = "Help";

    public static final Color BLACK;
    public static final Color WHITE;
    public static final Color CUSTOM_COLOR;
    public static final Color PANEL_BACKGROUND_COLOR;
    public static final Color PANEL_FOREGROUND_COLOR;
    public static final Color PANEL_CARET_COLOR;

    //Actions
    public static String CREATE_NEW_FILENAME;
    public static String DOCS_PATH;

    public static Font PANEL_FONT;

    //Buttons
    public static final String BUTTON_NEW = "Start";
    public static final String BUTTON_SAVE = "Save";
    public static final String BUTTON_REMOVE = "Remove";

    static {

        TITLE = "Quick Notes";
        VERSION = "1.0.0";
        PREFERRED_WIDTH = 1000;
        PREFERRED_HEIGHT = 600;

        CREATE_NEW_FILENAME = "Call your file:";
        DOCS_PATH = "Documents";

        BLACK = Color.BLACK;
        WHITE = Color.WHITE;
        CUSTOM_COLOR = new Color(0x2a2a2a);
        PANEL_BACKGROUND_COLOR = new Color(0x2a2a2a);
        PANEL_FOREGROUND_COLOR = Color.WHITE;
        PANEL_CARET_COLOR = Color.WHITE;
        PANEL_FONT = new Font("Courier New", Font.PLAIN, 12);
    }
}