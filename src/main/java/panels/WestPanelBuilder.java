package panels;

import contants.Constants;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;


public class WestPanelBuilder extends JPanel implements PanelBuilder {
    private DefaultListModel<String> model = new DefaultListModel<>();
    private JList<String> container = new JList<>(model);
    private JComboBox<String> comboBox = new JComboBox<>();

    private WestPanelBuilder(){
        super();
    }

    public static WestPanelBuilder getInstance(){
        return new WestPanelBuilder();
    }

    public PanelBuilder build(){
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,10,0,5));
        PanelBuilder.applyColors(this);
        PanelBuilder.applyColors(container);

        File[] directories = new File(Constants.DOCS_PATH).listFiles(File::isDirectory);
        for (File d: directories){
            comboBox.addItem(d.getName());
        }
        add(comboBox, BorderLayout.NORTH);
        add(container, BorderLayout.CENTER);

        container.getSelectionModel()
                .addListSelectionListener(event -> {
                    try {

                        byte[] bytes = Files.readAllBytes(PanelBuilder.getSelectedFilePath());
                        PanelBuilder.getMain().selectAll();
                        PanelBuilder.getMain().replaceSelection("");
                        PanelBuilder.getMain().append(new String(bytes));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

        comboBox.addActionListener(event -> {
            if(comboBox.getSelectedItem() != null){
                model.removeAllElements();
                File[] files = new File(PanelBuilder.getSelectedCategoryPath().toString()).listFiles(File::isFile);
                for (File f: files) {
                    model.addElement(f.getName());
                }

                PanelBuilder.getConsole().consoleWrite("Selected");

            }
        });
        return this;
    }

    public File[] getFiles(String category) {
        File[] files = new File(String.join("/", Constants.DOCS_PATH, category)).listFiles(File::isFile);
        return files;
    }

    public JList<String> getContainer(){
        return this.container;
    }

    public JComboBox<String> getComboBox() {
        return this.comboBox;
    }
}