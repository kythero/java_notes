package panels;


import contants.Constants;

import javax.swing.*;
import java.awt.*;


public class CenterPanelBuilder extends JPanel implements PanelBuilder {

    private JTextArea textArea = new JTextArea(5, 20);

    private CenterPanelBuilder(){
        super();
    }

    public static CenterPanelBuilder getInstance(){
        return new CenterPanelBuilder();
    }

    public PanelBuilder build(){
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,5,0,10));
        PanelBuilder.applyColors(this);
        PanelBuilder.applyColors(textArea);

        textArea.append("");

        JScrollPane js = new JScrollPane(textArea);
        js.setBorder(BorderFactory.createLineBorder(Constants.BLACK));
        add(js, BorderLayout.CENTER);
        return this;
    }

    public JTextArea getTextArea(){
        return this.textArea;
    }
}
