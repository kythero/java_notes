package panels;


import contants.Constants;

import javax.swing.*;
import java.nio.file.Path;
import java.nio.file.Paths;


public interface PanelBuilder {

    NorthPanelBuilder northPanel = (NorthPanelBuilder) NorthPanelBuilder.getInstance().build();
    WestPanelBuilder westPanel = (WestPanelBuilder) WestPanelBuilder.getInstance().build();
    CenterPanelBuilder centerPanel = (CenterPanelBuilder) CenterPanelBuilder.getInstance().build();
    SouthPanelBuilder southPanel = (SouthPanelBuilder) SouthPanelBuilder.getInstance().build();

    PanelBuilder build();

    static void applyColors(JTextArea component){
        component.setBackground(Constants.PANEL_BACKGROUND_COLOR);
        component.setForeground(Constants.PANEL_FOREGROUND_COLOR);
        component.setCaretColor(Constants.PANEL_CARET_COLOR);
        component.setFont(Constants.PANEL_FONT);
    }

    static void applyColors(JComponent component){
        component.setBackground(Constants.PANEL_BACKGROUND_COLOR);
        component.setForeground(Constants.PANEL_FOREGROUND_COLOR);
        component.setFont(Constants.PANEL_FONT);
    }

    static JComboBox<String> getComboBox(){
        return westPanel.getComboBox();
    }

    static SouthPanelBuilder getConsole(){
        return southPanel;
    }

    static JList<String> getList(){
        return westPanel.getContainer();
    }

    static JTextArea getMain(){
        return centerPanel.getTextArea();
    }

    static Path getSelectedFilePath() {
        return Paths.get(String.join("/",
                Constants.DOCS_PATH,
                PanelBuilder.getComboBox().getSelectedItem().toString(),
                PanelBuilder.getList().getSelectedValue()
        ));
    }

    static Path getSelectedCategoryPath() {
        return Paths.get(String.join("/",
                Constants.DOCS_PATH,
                PanelBuilder.getComboBox().getSelectedItem().toString()
        ));
    }
}
