package panels;


import javax.swing.*;
import java.awt.*;

public class SouthPanelBuilder extends JPanel implements PanelBuilder {

    private JTextArea console = new JTextArea(4,3);

    private SouthPanelBuilder(){
        super();
    }

    public static SouthPanelBuilder getInstance(){
        return new SouthPanelBuilder();
    }

    public PanelBuilder build(){
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(5,10,10,10));
        PanelBuilder.applyColors(this);
        PanelBuilder.applyColors(console);


        add(new JScrollPane(console), BorderLayout.CENTER);
        return this;
    }

    public void consoleAppend(String text){
        console.append(text + "\n");
    }

    public void consoleWrite(String text){
        consoleClear();
        consoleAppend(text);
    }

    public void consoleClear(){
        console.selectAll();
        console.replaceSelection("");
    }
}