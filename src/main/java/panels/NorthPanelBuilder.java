package panels;


import actions.NorthPanelActionImpl;
import contants.Constants;

import javax.swing.*;
import java.awt.*;


public class NorthPanelBuilder extends JPanel implements PanelBuilder {

    private JButton createCategory = new JButton(NorthPanelActionImpl.getInstance(Constants.MENU_NEW));
    private JButton createFile = new JButton(NorthPanelActionImpl.getInstance(Constants.MENU_NEW));
    private JButton removeCategory = new JButton(NorthPanelActionImpl.getInstance(Constants.BUTTON_REMOVE));
    private JButton removeFile = new JButton(NorthPanelActionImpl.getInstance(Constants.BUTTON_REMOVE));
    private JButton saveFile = new JButton(NorthPanelActionImpl.getInstance(Constants.BUTTON_SAVE));

    private NorthPanelBuilder(){
        super();
    }

    public static NorthPanelBuilder getInstance(){
        return new NorthPanelBuilder();
    }

    public PanelBuilder build(){
        setLayout(new FlowLayout(FlowLayout.LEFT));
        setBorder(BorderFactory.createEmptyBorder(10,10,5,10));
        PanelBuilder.applyColors(this);

        add(createCategory);
        add(createFile);
        add(removeCategory);
        add(removeFile);
        add(saveFile);

        return this;
    }
}
